
CC = g++

all: build

build: libbenchmark
# -lbenchmark -Llib -Ilib/include 
	$(CC) benchmark_test.cpp -lbenchmark -Llib -Ilib/include -o benchmark_test

libbenchmark:
	$(MAKE) -f lib/Makefile
	
clean_lib:
	$(MAKE) -f lib/Makefile clean

.PHONY: libbenchmark clean_lib

clean: clean_lib
	rm benchmark_test