/*
 * statistics.cpp
 *
 *  Created on: May 16, 2018
 *      Author: Jordan.Montgomery
 */

#include "../include/statistics.hpp"
#include "../include/benchmark.h"
#include <vector>
#include <map>
#include <limits> 


class Benchmark {
	private:
		std::vector<double> samples;
		int n_samples;
		double k, incr_mean, incr_var, max, min;
        TIME_TYPE start_time;
    
	public:
		Benchmark() : k(0.0), incr_mean(0.0), incr_var(0.0), max(0.0), min(std::numeric_limits<double>::max()) { }
		void add_sample(double sample) {
			this->samples.push_back(sample);
			this->n_samples += 1;

            double oldMean = this->incr_mean;
			this->incr_mean = this->incr_mean + (sample - this->incr_mean) / this->n_samples;
            this->incr_var  = this->incr_var  + (sample - this->incr_mean) * (sample - oldMean);
            
            this->min = std::min(sample, this->min);
            this->max = std::max(sample, this->max);
		}
        
        void tick() { 
            this->start_time = get_time();
        }
        
        ErrStt tock()
        {
            TIME_TYPE end_time = get_time();
            add_sample(time_diff(this->start_time, end_time));
            return SUCCESS;
        }

		Benchmark_Context get_context() {
			Benchmark_Context context;
			context.mean_time = this->incr_mean;
			context.variance  = this->incr_var / (this->n_samples-1);
			context.n_samples = this->n_samples;
            context.max       = this->max;
            context.min       = this->min;
			context.samples   = this->samples.data();
			return context;
		}
};

static std::map<Benchmark_Handle, Benchmark *> benchmarks;
static int id = 0;

Benchmark_Handle Initialize_statistics(void)
{
    // Could use std::move to avoid a copy into the map
	benchmarks[++id] = new Benchmark();
	return id;
}

void tick(Benchmark_Handle id)
{
	Benchmark *bm = benchmarks[id];
	bm->tick();
}

ErrStt tock(Benchmark_Handle id)
{
	Benchmark *bm = benchmarks[id];
	return bm->tock();
}

Benchmark_Context Get_Statistics(Benchmark_Handle id)
{
	Benchmark *bm = benchmarks[id];
	return bm->get_context();
}




