/*
 * benchmark.h
 *
 *  Created on: May 15, 2018
 *      Author: Jordan.Montgomery
 */

#ifndef BENCHMARK_H_
#define BENCHMARK_H_

#include <stdint.h>
#include <time.h>
#include "statistics.hpp"

#if defined(NUCLEUS) || defined(__linux__)
#define TIME_TYPE uint64_t 
#else
#error "System not supported"
#endif


#define BENCHMARK(iter, f, ...) ({ 	Benchmark_Handle bh = Initialize_statistics(); \
									printf("iterations: %i\n", iter);              \
									for(int i = 0; i < iter; i++) {                \
										tick(bh);                                   \
										f(__VA_ARGS__);                            \
										tock(bh);                                   \
									}                                              \
									Get_Statistics(bh);                            \
								})

#define BENCHMARK_NEW_RUN()  Initialize_statistics(x)
#define BENCHMARK_START_T(x) tick(x)
#define BENCHMARK_END_T(x)	 tock(x)
#define BENCHMARK_RESULTS(x) Get_Statistics(x)


TIME_TYPE __attribute__((weak)) get_time(void)
{
    #ifdef NUCLEUS
    return ESAL_PR_TMR_READ();
    #elif __linux__
    TIME_TYPE lo, hi;
    __asm__ __volatile__("rdtsc" : "=a" (lo), "=d" (hi));
    return ((TIME_TYPE) hi << 32ULL) | lo;
    #endif
}

double __attribute__((weak)) time_diff(TIME_TYPE start, TIME_TYPE end)
{
    #ifdef NUCLEUS
    const double freq = (double)(396*1000000);

	if(start < end)
		return ((double)(end - start)) / freq;
	else // Counter has overflowed
		return ((double)( (-1) - start) + end) / freq;
    #elif __linux__
    return (double)(end - start);
    #endif
}

#endif /* BENCHMARK_H_ */
