/*
 * statistics.hpp
 *
 *  Created on: May 16, 2018
 *      Author: Jordan.Montgomery
 */

#ifndef STATISTICS_HPP_
#define STATISTICS_HPP_

typedef enum ErrStt {
	SUCCESS = 0,
	GENERAL_ERROR = 1
} ErrStt;

typedef int Benchmark_Handle;

typedef struct Benchmark_Context {
	double mean_time;
	double variance;
	int n_samples;
    double max;
    double min;
	double *samples;
} Benchmark_Context;

extern "C" Benchmark_Handle Initialize_statistics(void);
extern "C" void tick(Benchmark_Handle id);
extern "C" ErrStt tock(Benchmark_Handle id);
extern "C" Benchmark_Context Get_Statistics(Benchmark_Handle id);

#endif /* STATISTICS_HPP_ */
