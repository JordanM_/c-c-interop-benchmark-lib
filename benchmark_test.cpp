#include <stdio.h>
#include "lib/include/benchmark.h"
#include <unistd.h>

static int print_line_separator()
{
	//std::this_thread::sleep_for(std::chrono::milliseconds(100));
    return 2+2;
}

int main(int argc, char **argv)
{

    printf("\r\nHello Nucleus world!\r\n");

    Benchmark_Context bm = BENCHMARK(10000, print_line_separator);
    printf("mean: %f \nsamples: %i \nvariance: %f \nmax: %f \nmin: %f\n\n", bm.mean_time, bm.n_samples, bm.variance, bm.max, bm.min);

    for(int i = 0; i<100; i++)
    {
    	printf("sample %i: %f\n", i, bm.samples[i]);
    }

	return 0;
}